from django.shortcuts import render
from rest_framework import status
from django.http import JsonResponse
from .models import Student, Department
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import StudentSerializer
# Create your views here.

@api_view(['GET'])
def home(request):
    blogs = Student.objects.all()
    serializer = StudentSerializer(blogs,many=True)
    return Response(serializer.data,status=status.HTTP_200_OK)

@api_view(['GET'])
def get_value(request,pk):
    get_blog = Student.objects.get(id=pk)
    serializer = StudentSerializer(get_blog)
    return Response(serializer.data,status=status.HTTP_200_OK)

@api_view(['POST'])
def add_blog(request):
    serializer = StudentSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data,status=status.HTTP_201_CREATED)
    return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

@api_view(['PUT','PATCH','DELETE'])
def update_blog(request,pk):
    blog = Student.objects.get(id=pk)
    if request.method =="PUT":
        serializer = StudentSerializer(blog, data=request.data)
        
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_200_OK)
        else:
            return Response (serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    if request.method =="PATCH":
        serializer = StudentSerializer(blog, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
    
    if request.method =="DELETE":
        blog.delete()
        return Response({"message":"Blog has been deleted"},status=status.HTTP_204_NO_CONTENT)
    
