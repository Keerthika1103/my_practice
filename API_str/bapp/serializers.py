from rest_framework import serializers
from .models import Student, Department
class StudentSerializer(serializers.ModelSerializer):
    student = serializers.SlugRelatedField(many=True,read_only=True,slug_field='description')
    class Meta:
        model = Student
        fields = ['name','email','student']