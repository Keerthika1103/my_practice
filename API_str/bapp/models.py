from typing import Any
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.template.defaultfilters import slugify

class Student(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)

   
    def __str__(self):
        return self.name

class Department(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE,related_name='student')
    roll = models.IntegerField()
    department = models.CharField(max_length=100)
    description = models.TextField()

    class Meta:
        unique_together = ['student', 'roll']
        ordering = ['roll']
    def __str__(self):
        return '%d: %s' %(self.roll, self.department)
    
       
















































# Create your models here.
# class Blog(models.Model):
#     name          = models.CharField(max_length=100)
#     author        = models.CharField(max_length=100)
#     description   = models.TextField()
#     post_date     = models.DateTimeField(default=datetime.today)
#     slug          = models.CharField(max_length=1000, blank=True, null=True)

#     def __str__(self):
#         return self.name +"==>"+str(self.description)    

#     def save(self, *args, **kwargs):
#         if not self.slug:
#             self.slug = slugify(self.name+"-"+str(self.post_date))
#         return super(Blog,self).save(*args, **kwargs)
    
# class BlogComment(models.Model):
#     description     = models.TextField()
#     author          = models.ForeignKey(User, on_delete=models.SET_NULL,null=True)
#     comment_date    = models.DateTimeField(auto_now_add=True)
#     blog            = models.ForeignKey(Blog, on_delete=models.CASCADE)

#     def __str__(self):
#         return str(self.blog)
     

# class BlogComment(models.Model):
#     description     = models.TextField()
#     comment_date    = models.DateTimeField(auto_now_add=True)
#     blog          = models.ForeignKey(Blog, on_delete=models.CASCADE,null=True,related_name='blog')

#     def __str__(self):
#         return str(self.blog)
