from django.contrib import admin
from .models import Student, Department
# Register your models here.

# class BlogAdmin(admin.ModelAdmin):
#     list_display = ('name', 'author', 'description', 'post_date', 'slug')
#     list_display_links = ('name', 'author')
#     search_fields = ('name', 'author')
#     list_per_page   = 1
    # list_editable = ('post_date',)

admin.site.register(Student)
admin.site.register(Department)