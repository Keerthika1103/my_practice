from django.urls import path
from bapp import views

urlpatterns = [
    path('',views.home,name='home'),
    path('get_value/<int:pk>/',views.get_value,name='get_value'),
    path('add_blog/',views.add_blog,name='add_blog'),
    path('update_blog/<int:pk>/',views.update_blog,name='update_blog'),
    
]
