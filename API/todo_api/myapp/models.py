from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Tasklist(models.Model):
    user    = models.CharField(max_length=200)
    title   = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    created  = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    