from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import Tasklist
from .serializers import TaskSerializer
# Create your views here.
@api_view(['GET'])
def index(request):
    task_data = Tasklist.objects.all()
    serializer = TaskSerializer(task_data, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def create(request):
    serializer = TaskSerializer(data=request.data)
    task_db = Tasklist.objects.all()

    my_ser = TaskSerializer(task_db,many=True)

    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    else:
        return Response(serializer.errors)

@api_view(['GET'])
def get_task(request,pk):
    task_data = Tasklist.objects.get(id=pk)
    serializer = TaskSerializer(task_data)
    return Response({"Message":serializer.data})

@api_view(['PUT','DELETE','PATCH'])
def update(request,pk):
    task_data = Tasklist.objects.get(id=pk)
   
    if request.method == 'PUT':
        my_serializer = TaskSerializer(task_data ,data=request.data)

        if my_serializer.is_valid():
            my_serializer.save()
            return Response(my_serializer.data)
        else:
            return Response(my_serializer.errors)
        
    elif request.method == 'PATCH':
        my_serializer = TaskSerializer(task_data,data=request.data,partial=True)

        if my_serializer.is_valid():
            my_serializer.save()
            return Response({"Message":"Your Data has updated (PATCH)"})
        else:
            return Response(my_serializer.errors)

    else:
        task_data.delete()
        return Response({"Message":"Your Data has deleted"})
