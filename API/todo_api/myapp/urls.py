from django.urls import path
from .import views
urlpatterns = [
    path('',views.index,name='index'),
    path('create',views.create,name='create'),
    path('get_task/<int:pk>',views.get_task,name='get_task'),
    path('update/<int:pk>',views.update,name='update'),
]
         
