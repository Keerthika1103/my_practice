from django.urls import path, include
from . import views
urlpatterns = [
    path('',views.index,name='index'),
    path('create',views.create,name='create'),
    path('get_blog/<int:pk>',views.get_blog,name='get_blog'),
    path('update_blog/<int:pk>',views.update_blog,name='update_blog'),
]
