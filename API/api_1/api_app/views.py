from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import BlogModel
from .serializers import BlogModelSerializer
# Create your views here.

@api_view(['GET'])
def index(request):
    blog = BlogModel.objects.all()
    serializer = BlogModelSerializer(blog, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def create(request):
    serializer = BlogModelSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors)

@api_view(['GET'])
def get_blog(request, pk):
    blog = BlogModel.objects.get(pk=pk)
    serializer = BlogModelSerializer(blog)
    return Response(serializer.data)

@api_view(['PUT','PATCH'])
def update_blog(request, pk):
    blog = BlogModel.objects.get(pk=pk)
    serializer = BlogModelSerializer(blog, data=request.data,partial=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors)