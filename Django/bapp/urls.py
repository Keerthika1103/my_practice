from django.urls import path
from . import views

urlpatterns = [
    path('',views.index,name="index"),
    path('blog_Post/',views.BlogCreate.as_view(),name="BlogPost"),
    path('blog_list/',views.BlogList.as_view(),name="BlogList"),
    path('blog_detail/<int:pk>/',views.BlogDetail.as_view(),name="BlogDetail"),
    path('blog_update/<int:pk>/',views.BlogUpdate.as_view(),name="BlogUpdate"),
    path('blog_delete/<int:pk>/',views.BlogDelete.as_view(),name="BlogDelete"),
]
