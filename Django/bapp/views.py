from django.shortcuts import render
from bapp.models import BlogModel
from bapp.forms import BlogForm
from django.views.generic import ListView,DetailView,CreateView,UpdateView,DeleteView
from django.urls import reverse_lazy
# Create your views here

def index(request):
    return render(request,'index.html')

# def BlogPost(request):
#     if request.method == 'POST':
#         form = BlogForm(request.POST, request.FILES)
#         if form.is_valid():
#             form.save()
#             return render(request,'Blog.html',{'form':form})
#         else:
#             form = BlogForm()
#     return render(request,'Blog.html')

class BlogCreate(CreateView):
    model = BlogModel
    form_class = BlogForm
    template_name = 'Blog.html'
    success_url = reverse_lazy('index')

class BlogList(ListView):
    model = BlogModel
    template_name = 'blog_list.html'
    context_object_name = 'data'

class BlogDetail(DetailView):
    model = BlogModel
    template_name = 'blog_detail.html'
    context_object_name = 'data'

class BlogUpdate(UpdateView):
    model = BlogModel
    form_class = BlogForm
    template_name = 'Blog.html'
    success_url = reverse_lazy('index')

class BlogDelete(DeleteView):
    model = BlogModel
    template_name = 'blog_delete.html'
    context_object_name = 'data'
    success_url = reverse_lazy('index')
    