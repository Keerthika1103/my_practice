from .models import BlogModel
from django import forms

class BlogForm(forms.ModelForm):
        class Meta:
            model = BlogModel
            fields = '__all__'
