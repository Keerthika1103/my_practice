from django.db import models

# Create your models here.
class Blog(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    image = models.ImageField(upload_to='pics',blank=True,null=True)
    created_at = models.DateField(auto_now_add=True)

    