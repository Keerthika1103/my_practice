from bapp import views
from django.urls import path
from .views import contact_us,blog_detail

urlpatterns = [
    path('',views.homepage,name="homepage"),
    path('contact_us',views.contact_us,name="contact_us"),
    path('blog_detail',views.blog_detail,name="blog_detail"),
    path('signup',views.signup,name="signup"),
    path('user_login',views.user_login,name="login"),
    path('user_logout',views.user_logout,name="logout"),

]
