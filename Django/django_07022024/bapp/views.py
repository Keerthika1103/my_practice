from django.shortcuts import render,HttpResponse,redirect
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout

# Create your views here.

def homepage(request):
    return render(request,'homepage.html')

def blog_detail(request):
    return render(request,'blog_detail.html')

def profile(request):
    return render(request,'')

def contact_us(request):
    return render(request,'contact_us.html')

def signup(request):

    if request.method =='POST':
        username = request.POST.get('username')
        email  = request.POST.get('email')
        password = request.POST.get('pass1')
        confirm_pasword  = request.POST.get('pass2')

        if password != confirm_pasword:
            messages.warning(request,"The passwords do not match")
            return redirect('/signup')
        
        try:
            if User.objects.get(username=username):
                messages.warning(request,"The username already exists")
                return redirect('/signup')
        except:
            pass

        try:
            if User.objects.filter(email=email).exists():
                messages.warning(request,"The email already exists")
                return redirect('/signup')
        except:
            pass

        myuser = User.objects.create_user(username,email,password)
        myuser.save()
        messages.warning(request,"Your account has been created successfully")
        return redirect('/')

    return render(request,'signup.html')

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        myuser = authenticate(username=username,password=password)
        if myuser is not None:
            login(request,myuser)
            messages.success(request,"Logged in successfully")
            return redirect('/')
        else:
            messages.error(request,"Invalid credentials")
            return redirect('/login')

    return render(request,'login.html')

def user_logout(request):
    logout(request)
    messages.success(request,"Logged out successfully")
    return redirect('/')